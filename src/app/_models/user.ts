﻿export class User {
    id?: number;
    name?: string;
    username?: string;
    email?: string;
    age?: string;
    token?: string;
}