export interface Game {
  id: string;
  title: string;
  description: string;
  price: number;
  favourite: boolean;
  tag:string
}
