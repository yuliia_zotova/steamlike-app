import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core'
import { Game } from '../_models/game';


const BASE_URL = 'http://localhost:3000'

@Injectable({
  providedIn: 'root'
})
export class GamesService {
 model = 'games';
 
 constructor(private http: HttpClient) {}

 all() {
   return this.http.get(this.getUrl());
 }

 private getUrl() {
   return `${BASE_URL}/${this.model}`;
 }
}
