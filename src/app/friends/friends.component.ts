import { Component, OnInit } from '@angular/core';
import { FriendsService } from '../_services/friends.service';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.scss']
})
export class FriendsComponent implements OnInit {
  search: string | undefined;
  searchFriends: any[] | undefined;
  users:any = [];
  friends: any = [];
  

  constructor(private friendService:FriendsService) { }

  ngOnInit(): void {
    this.fetchUsers();
    
  }
fetchUsers() {
  this.friendService.all()
  .subscribe((result:any) => {
    this.users = result
    this.searchFriends = this.users;
    this.filter()
  })
}
  filter():void {
    this.friends = this.users.filter((user: { friend: boolean; }) => user.friend === true)
    this.searchFriends = this.users.filter((item:any) => {
      if (!item.friend) {
        return item.name.includes(this.search) || item.email.includes(this.search) 
      }

    })
  }

  showResult():void {
    let hidden = document.getElementById("friendsSearch");
    hidden?.classList.remove('hidden')
  }

 addFriend(id: any){
   this.users[id-1].friend = true
   this.filter();
 }
 removeFriend(id: any){
  this.users[id-1].friend = false
  this.filter();
}
}
