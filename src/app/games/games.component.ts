import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { GamesService } from '../_services/games.service';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss'],
})
export class GamesComponent implements OnInit {
  search: string | undefined;
  slider: number | undefined;
  test: any[] | undefined;
  games: any = [];
  filteredGames: any[] | undefined;

  checkboxes: any = {
    indie: false,
    action: false,
    adventure: false,
  };

  constructor(private gamesService: GamesService) { }

  ngOnInit(): void {
    this.fetchGames();
    this.slider = 0;

  }

  fetchGames() {
    this.gamesService.all()
      .subscribe((result: any) => {
        this.games = result
        this.filteredGames = this.games;
        this.filter()
      })

  }

  addGame(id:any) {
    this.games[id-1].favourite = true;
    this.filter()
  }

  filter() {
    let filteredGames = this.games;
    let filterByPrice = this.slider !== undefined && this.slider !== 0;
    if (filterByPrice) {
      filteredGames = filteredGames.filter((item: any) => {
        return this.slider && item.price <= this.slider;
      });
    }

    let filterByTag = false;
    for (const tag in this.checkboxes) {
      if (this.checkboxes[tag] === true) {
        filterByTag = true;
      }
    }

    if (filterByTag) {
      filteredGames = filteredGames.filter((item: any) => {
        let tagFilterResult = false;
        let gameTag = item.tag;
        for (const tag in this.checkboxes) {
          if (this.checkboxes[tag] === true) {
            if (gameTag.toLowerCase() === tag) {
              tagFilterResult = true;
            }
          }
        }

        return tagFilterResult;
      });
    }

    let filterByName = this.search !== undefined && this.search !== '';
    if (filterByName) {
      filteredGames = filteredGames.filter((item: any) => {
        let titleCaseHandler = item.title.toLowerCase();
        let descriptionCaseHandler = item.description.toLowerCase();
        let searchCaseHandler = this.search?.toLowerCase();

        if (searchCaseHandler) {
          return (
            titleCaseHandler.includes(searchCaseHandler) ||
            descriptionCaseHandler.includes(searchCaseHandler)
          );
        }
      });
    }

    this.filteredGames = filteredGames;
  }
}
