import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from './_services';
import { User } from './_models';
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'Steamlike App';
  user?: User;
  links = [
    { path: '/games', icon: 'view_list', title: 'Games' },
    { path: '/library', icon: 'view_list', title: 'Library' },
    { path: '/friends', icon: 'view_list', title: 'Friends' },
    { path: '/profile', icon: 'view_list', title: 'Profile' },
  ];

  constructor(private authenticationService: AuthenticationService) {
    this.authenticationService.user.subscribe(x => this.user = x);
}

logout() {
    this.authenticationService.logout();
}
}

export class HomeComponent {
    

    
}