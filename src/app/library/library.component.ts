import { Component, OnInit } from '@angular/core';
import { GamesService } from '../_services/games.service';
@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.scss']
})
export class LibraryComponent implements OnInit {
  games:any = [];
  favouriteGames: any[] | undefined = [];

  constructor(private gamesService:GamesService) { }

  ngOnInit(): void {
  this.fetchGames()
  }

  fetchGames() {
    this.gamesService.all()
    .subscribe((result:any) => {
      this.games = result
      this.filterFavourite()
    })
  }

  filterFavourite() {
   for(let game of this.games) {
     if (game.favourite) {
       this.favouriteGames?.push(game)
     }
   }
  }

}
